﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Network
{
    public static class Extensions
    {
        public static List<byte[]> Split(this byte[] array, byte split)
        {
            var splittedBytes = new List<byte[]>();

            var index = 0;
            while ((index = Array.IndexOf(array, split)) >= 0)
            {
                var tmp = new byte[index];
                Array.Copy(array, tmp, index);

                splittedBytes.Add(tmp);

                array = array.ResizeArrayFromBehind(array.Length - index - 1);
            }

            Array.Clear(array, 0, array.Length);
            return splittedBytes;
        }

        public static byte[] ResizeArrayFromBehind(this byte[] array, int size)
        {
            var tmp = new byte[size];
            Array.Reverse(array);
            Array.Copy(array, tmp, size);

            Array.Reverse(tmp);
            return tmp;
        }

        public static Packet ToPacket(this string message, bool isReceivedPacked = false)
        {
            if (isReceivedPacked)
                return Packet.Parse(Encoding.UTF8.GetBytes(message));
            else
                return new Packet(message);
        }
    }
}
