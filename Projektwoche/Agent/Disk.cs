﻿using System;
using System.Linq;
using System.IO;

namespace Agent
{
    class Disk
    {
        public string Letter { get; private set; }
        public string Name { get; private set; }
        public double Space { get; private set; }
        public double FreeSpace { get; private set; }

        //Klasse mit statischen Methoden Objekt wird später übergeben in die Data Klasse für das Serialisieren
        public Disk(string Letter, string Name, double Space, double FreeSpace)
        {
            this.Letter = Letter;
            this.Name = Name;
            this.Space = Space;
            this.FreeSpace = FreeSpace;
        }

        public static Disk[] GetDisks(DriveType type = DriveType.Fixed)
        {
            Disk[] disks = new Disk[GetNumber(type)];

            DriveInfo[] Drives = DriveInfo.GetDrives();
            int i = 0;
            foreach (DriveInfo item in Drives)
            {
                if (item.IsReady && item.DriveType == type)
                    disks[i++] = new Disk(item.Name, item.VolumeLabel, item.TotalSize / 1073741824, item.TotalFreeSpace / 1073741824);
            }

            return disks;
        }

        public static double GetDiskSpace(string Laufwerksbuchstaben = "C")
        {
            string Laufwerk = String.Format(@"{0}:\", Laufwerksbuchstaben);

            DriveInfo[] Drives = DriveInfo.GetDrives();
            DriveInfo Drive = Drives.Where(x => x.Name == Laufwerk).SingleOrDefault();

            return Drive.TotalSize / 1073741824;
        }

        public static double GetDiskFree(string Laufwerksbuchstaben = "C")
        {
            string Laufwerk = String.Format(@"{0}:\", Laufwerksbuchstaben);

            DriveInfo[] Drives = DriveInfo.GetDrives();
            DriveInfo Drive = Drives.Where(x => x.Name == Laufwerk).SingleOrDefault();

            return Drive.TotalFreeSpace / 1073741824;
        }

        private static int GetNumber(DriveType type = DriveType.Fixed)
        {
            int number = 0;

            DriveInfo[] Drives = DriveInfo.GetDrives();
            foreach (DriveInfo item in Drives)
            {
                if (item.IsReady && item.DriveType == type)
                    number++;
            }

            return number;
        }
    }
}
