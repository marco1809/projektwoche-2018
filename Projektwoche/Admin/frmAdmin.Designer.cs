﻿namespace Admin
{
    partial class FrmAdmin
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.StatusStrip = new System.Windows.Forms.StatusStrip();
            this.lbConnectionStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnConnect = new System.Windows.Forms.Button();
            this.cCpu = new LiveCharts.WinForms.CartesianChart();
            this.timDataRequest = new System.Windows.Forms.Timer(this.components);
            this.cRam = new LiveCharts.WinForms.CartesianChart();
            this.cNetwork = new LiveCharts.WinForms.CartesianChart();
            this.lblCpu = new System.Windows.Forms.Label();
            this.lblRam = new System.Windows.Forms.Label();
            this.lblDisk = new System.Windows.Forms.Label();
            this.lblNetwork = new System.Windows.Forms.Label();
            this.sg2 = new LiveCharts.WinForms.SolidGauge();
            this.sgDisk1 = new LiveCharts.WinForms.SolidGauge();
            this.cbAgent = new System.Windows.Forms.ComboBox();
            this.lblAgent = new System.Windows.Forms.Label();
            this.btnChoose = new System.Windows.Forms.Button();
            this.lblProcessorCount = new System.Windows.Forms.Label();
            this.lblProcessorCountValue = new System.Windows.Forms.Label();
            this.lblCoresValue = new System.Windows.Forms.Label();
            this.lblCores = new System.Windows.Forms.Label();
            this.lblClockSpeedValue = new System.Windows.Forms.Label();
            this.lblClockSpeed = new System.Windows.Forms.Label();
            this.lblMaxClockSpeedValue = new System.Windows.Forms.Label();
            this.lblMaxClockSpeed = new System.Windows.Forms.Label();
            this.lblDiskTitle1 = new System.Windows.Forms.Label();
            this.lblDiskTitle2 = new System.Windows.Forms.Label();
            this.StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // StatusStrip
            // 
            this.StatusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbConnectionStatus});
            this.StatusStrip.Location = new System.Drawing.Point(0, 550);
            this.StatusStrip.Name = "StatusStrip";
            this.StatusStrip.Size = new System.Drawing.Size(1216, 22);
            this.StatusStrip.TabIndex = 1;
            // 
            // lbConnectionStatus
            // 
            this.lbConnectionStatus.Name = "lbConnectionStatus";
            this.lbConnectionStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(629, 500);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(550, 47);
            this.btnConnect.TabIndex = 2;
            this.btnConnect.Text = "Verbinden";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // cCpu
            // 
            this.cCpu.Location = new System.Drawing.Point(12, 30);
            this.cCpu.Name = "cCpu";
            this.cCpu.Size = new System.Drawing.Size(550, 218);
            this.cCpu.TabIndex = 4;
            this.cCpu.Text = "CPU Auslastung";
            // 
            // timDataRequest
            // 
            this.timDataRequest.Enabled = true;
            this.timDataRequest.Interval = 5000;
            this.timDataRequest.Tick += new System.EventHandler(this.timDataRequest_Tick);
            // 
            // cRam
            // 
            this.cRam.Location = new System.Drawing.Point(629, 30);
            this.cRam.Name = "cRam";
            this.cRam.Size = new System.Drawing.Size(550, 218);
            this.cRam.TabIndex = 5;
            this.cRam.Text = "Ram Auslastung";
            // 
            // cNetwork
            // 
            this.cNetwork.Location = new System.Drawing.Point(629, 278);
            this.cNetwork.Name = "cNetwork";
            this.cNetwork.Size = new System.Drawing.Size(550, 216);
            this.cNetwork.TabIndex = 7;
            this.cNetwork.Text = "Netzwerk Auslastung";
            // 
            // lblCpu
            // 
            this.lblCpu.AutoSize = true;
            this.lblCpu.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCpu.Location = new System.Drawing.Point(207, 9);
            this.lblCpu.Name = "lblCpu";
            this.lblCpu.Size = new System.Drawing.Size(146, 24);
            this.lblCpu.TabIndex = 8;
            this.lblCpu.Text = "CPU Auslastung";
            // 
            // lblRam
            // 
            this.lblRam.AutoSize = true;
            this.lblRam.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRam.Location = new System.Drawing.Point(833, 9);
            this.lblRam.Name = "lblRam";
            this.lblRam.Size = new System.Drawing.Size(150, 24);
            this.lblRam.TabIndex = 9;
            this.lblRam.Text = "RAM Auslastung";
            // 
            // lblDisk
            // 
            this.lblDisk.AutoSize = true;
            this.lblDisk.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisk.Location = new System.Drawing.Point(119, 251);
            this.lblDisk.Name = "lblDisk";
            this.lblDisk.Size = new System.Drawing.Size(204, 24);
            this.lblDisk.TabIndex = 10;
            this.lblDisk.Text = "Belegter Speicher (GB)";
            // 
            // lblNetwork
            // 
            this.lblNetwork.AutoSize = true;
            this.lblNetwork.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNetwork.Location = new System.Drawing.Point(819, 251);
            this.lblNetwork.Name = "lblNetwork";
            this.lblNetwork.Size = new System.Drawing.Size(186, 24);
            this.lblNetwork.TabIndex = 11;
            this.lblNetwork.Text = "Netzwerk Auslastung";
            // 
            // sg2
            // 
            this.sg2.Location = new System.Drawing.Point(211, 278);
            this.sg2.Name = "sg2";
            this.sg2.Size = new System.Drawing.Size(181, 193);
            this.sg2.TabIndex = 12;
            // 
            // sgDisk1
            // 
            this.sgDisk1.Location = new System.Drawing.Point(12, 278);
            this.sgDisk1.Name = "sgDisk1";
            this.sgDisk1.Size = new System.Drawing.Size(180, 193);
            this.sgDisk1.TabIndex = 13;
            // 
            // cbAgent
            // 
            this.cbAgent.Enabled = false;
            this.cbAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAgent.FormattingEnabled = true;
            this.cbAgent.Location = new System.Drawing.Point(12, 500);
            this.cbAgent.Name = "cbAgent";
            this.cbAgent.Size = new System.Drawing.Size(387, 24);
            this.cbAgent.TabIndex = 14;
            this.cbAgent.SelectedIndexChanged += new System.EventHandler(this.cbAgent_SelectedIndexChanged);
            // 
            // lblAgent
            // 
            this.lblAgent.AutoSize = true;
            this.lblAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgent.Location = new System.Drawing.Point(8, 460);
            this.lblAgent.Name = "lblAgent";
            this.lblAgent.Size = new System.Drawing.Size(161, 24);
            this.lblAgent.TabIndex = 15;
            this.lblAgent.Text = "Agent auswählen:";
            // 
            // btnChoose
            // 
            this.btnChoose.Location = new System.Drawing.Point(405, 500);
            this.btnChoose.Name = "btnChoose";
            this.btnChoose.Size = new System.Drawing.Size(218, 47);
            this.btnChoose.TabIndex = 17;
            this.btnChoose.Text = "Agent auswählen";
            this.btnChoose.UseVisualStyleBackColor = true;
            // 
            // lblProcessorCount
            // 
            this.lblProcessorCount.AutoSize = true;
            this.lblProcessorCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProcessorCount.Location = new System.Drawing.Point(402, 329);
            this.lblProcessorCount.Name = "lblProcessorCount";
            this.lblProcessorCount.Size = new System.Drawing.Size(92, 17);
            this.lblProcessorCount.TabIndex = 18;
            this.lblProcessorCount.Text = "Prozessoren:";
            // 
            // lblProcessorCountValue
            // 
            this.lblProcessorCountValue.AutoSize = true;
            this.lblProcessorCountValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProcessorCountValue.Location = new System.Drawing.Point(500, 329);
            this.lblProcessorCountValue.Name = "lblProcessorCountValue";
            this.lblProcessorCountValue.Size = new System.Drawing.Size(0, 17);
            this.lblProcessorCountValue.TabIndex = 19;
            // 
            // lblCoresValue
            // 
            this.lblCoresValue.AutoSize = true;
            this.lblCoresValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoresValue.Location = new System.Drawing.Point(500, 278);
            this.lblCoresValue.Name = "lblCoresValue";
            this.lblCoresValue.Size = new System.Drawing.Size(0, 17);
            this.lblCoresValue.TabIndex = 21;
            // 
            // lblCores
            // 
            this.lblCores.AutoSize = true;
            this.lblCores.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCores.Location = new System.Drawing.Point(402, 278);
            this.lblCores.Name = "lblCores";
            this.lblCores.Size = new System.Drawing.Size(50, 17);
            this.lblCores.TabIndex = 20;
            this.lblCores.Text = "Kerne:";
            // 
            // lblClockSpeedValue
            // 
            this.lblClockSpeedValue.AutoSize = true;
            this.lblClockSpeedValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClockSpeedValue.Location = new System.Drawing.Point(500, 295);
            this.lblClockSpeedValue.Name = "lblClockSpeedValue";
            this.lblClockSpeedValue.Size = new System.Drawing.Size(0, 17);
            this.lblClockSpeedValue.TabIndex = 25;
            // 
            // lblClockSpeed
            // 
            this.lblClockSpeed.AutoSize = true;
            this.lblClockSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClockSpeed.Location = new System.Drawing.Point(402, 295);
            this.lblClockSpeed.Name = "lblClockSpeed";
            this.lblClockSpeed.Size = new System.Drawing.Size(89, 17);
            this.lblClockSpeed.TabIndex = 24;
            this.lblClockSpeed.Text = "Clock speed:";
            // 
            // lblMaxClockSpeedValue
            // 
            this.lblMaxClockSpeedValue.AutoSize = true;
            this.lblMaxClockSpeedValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxClockSpeedValue.Location = new System.Drawing.Point(500, 312);
            this.lblMaxClockSpeedValue.Name = "lblMaxClockSpeedValue";
            this.lblMaxClockSpeedValue.Size = new System.Drawing.Size(0, 17);
            this.lblMaxClockSpeedValue.TabIndex = 27;
            // 
            // lblMaxClockSpeed
            // 
            this.lblMaxClockSpeed.AutoSize = true;
            this.lblMaxClockSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxClockSpeed.Location = new System.Drawing.Point(402, 312);
            this.lblMaxClockSpeed.Name = "lblMaxClockSpeed";
            this.lblMaxClockSpeed.Size = new System.Drawing.Size(75, 17);
            this.lblMaxClockSpeed.TabIndex = 26;
            this.lblMaxClockSpeed.Text = "Max Clock:";
            // 
            // lblDiskTitle1
            // 
            this.lblDiskTitle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiskTitle1.Location = new System.Drawing.Point(12, 278);
            this.lblDiskTitle1.Name = "lblDiskTitle1";
            this.lblDiskTitle1.Size = new System.Drawing.Size(180, 24);
            this.lblDiskTitle1.TabIndex = 28;
            this.lblDiskTitle1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDiskTitle2
            // 
            this.lblDiskTitle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiskTitle2.Location = new System.Drawing.Point(211, 278);
            this.lblDiskTitle2.Name = "lblDiskTitle2";
            this.lblDiskTitle2.Size = new System.Drawing.Size(181, 24);
            this.lblDiskTitle2.TabIndex = 29;
            this.lblDiskTitle2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1216, 572);
            this.Controls.Add(this.lblDiskTitle2);
            this.Controls.Add(this.lblDiskTitle1);
            this.Controls.Add(this.lblMaxClockSpeedValue);
            this.Controls.Add(this.lblMaxClockSpeed);
            this.Controls.Add(this.lblClockSpeedValue);
            this.Controls.Add(this.lblClockSpeed);
            this.Controls.Add(this.lblCoresValue);
            this.Controls.Add(this.lblCores);
            this.Controls.Add(this.lblProcessorCountValue);
            this.Controls.Add(this.lblProcessorCount);
            this.Controls.Add(this.btnChoose);
            this.Controls.Add(this.lblAgent);
            this.Controls.Add(this.cbAgent);
            this.Controls.Add(this.sgDisk1);
            this.Controls.Add(this.sg2);
            this.Controls.Add(this.lblNetwork);
            this.Controls.Add(this.lblDisk);
            this.Controls.Add(this.lblRam);
            this.Controls.Add(this.lblCpu);
            this.Controls.Add(this.cNetwork);
            this.Controls.Add(this.cRam);
            this.Controls.Add(this.cCpu);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.StatusStrip);
            this.Name = "FrmAdmin";
            this.Text = "Projektwoche 2018 - Administrator";
            this.Load += new System.EventHandler(this.FrmAdmin_Load);
            this.StatusStrip.ResumeLayout(false);
            this.StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lbConnectionStatus;
        private System.Windows.Forms.Button btnConnect;
        private LiveCharts.WinForms.CartesianChart cCpu;
        private System.Windows.Forms.Timer timDataRequest;
        private LiveCharts.WinForms.CartesianChart cRam;
        private LiveCharts.WinForms.CartesianChart cNetwork;
        private System.Windows.Forms.Label lblCpu;
        private System.Windows.Forms.Label lblRam;
        private System.Windows.Forms.Label lblDisk;
        private System.Windows.Forms.Label lblNetwork;
        private LiveCharts.WinForms.SolidGauge sg2;
        private LiveCharts.WinForms.SolidGauge sgDisk1;
        private System.Windows.Forms.ComboBox cbAgent;
        private System.Windows.Forms.Label lblAgent;
        private System.Windows.Forms.Button btnChoose;
        private System.Windows.Forms.Label lblProcessorCount;
        private System.Windows.Forms.Label lblProcessorCountValue;
        private System.Windows.Forms.Label lblCoresValue;
        private System.Windows.Forms.Label lblCores;
        private System.Windows.Forms.Label lblClockSpeedValue;
        private System.Windows.Forms.Label lblClockSpeed;
        private System.Windows.Forms.Label lblMaxClockSpeedValue;
        private System.Windows.Forms.Label lblMaxClockSpeed;
        private System.Windows.Forms.Label lblDiskTitle1;
        private System.Windows.Forms.Label lblDiskTitle2;
    }
}

