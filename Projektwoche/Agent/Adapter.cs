﻿using System;
using System.Threading;
using System.Net.NetworkInformation;


namespace Agent
{
    public class Adapter
    {
        public Netstat[] arrInterfaces;
        private Timer timreceivedspeed;
        private Timer timsendspeed;

        public Adapter()
        {
            //Hier werden alle Adapter ausgelesen und ein dynamisches Array aus der Klasse Netstate erzeugt, ausgesuchte


            var interfaces = NetworkInterface.GetAllNetworkInterfaces();
            arrInterfaces = new Netstat[interfaces.Length];

            //Alle Interfaces auslesen und dynamisches Array erstellen
            int i = 0;
            foreach (var item in interfaces)
            {
                arrInterfaces[i] = new Netstat();
                arrInterfaces[i].Name = item.Name;
                arrInterfaces[i].Mac = item.GetPhysicalAddress().ToString();
                arrInterfaces[i].ReceivedSpeed = 0;
                arrInterfaces[i].SendSpeed = 0;
                i++;
            }

            //Timerthread für die automatische aktualisierung auf den Client, alle 30 Sekunden
            timreceivedspeed = new System.Threading.Timer(new System.Threading.TimerCallback(GetReceivedSpeed), 1000, 0, 30000);
            timsendspeed = new System.Threading.Timer(new System.Threading.TimerCallback(GetSentSpeed), 1000, 0, 30000);
        }

        private void GetReceivedSpeed(object obj)
        {
            NetworkInterface[] Interfaces = NetworkInterface.GetAllNetworkInterfaces();

            int i = 0;
            foreach (var item in Interfaces)
            {
                //Read Data
                IPv4InterfaceStatistics interfaceStats = item.GetIPv4Statistics();
                int bytesReceived = (int)interfaceStats.BytesReceived;

                //Durschschnittsgeschwindigkeit pro Sekunde
                Thread.Sleep(1000);

                interfaceStats = item.GetIPv4Statistics();
                int newBytesReceived = (int)interfaceStats.BytesReceived;

                //Read Speed
                arrInterfaces[i].ReceivedSpeed = Math.Abs(newBytesReceived - bytesReceived) / 1048576D;
                i++;
            }
        }

        private void GetSentSpeed(object obj)
        {
            NetworkInterface[] Interfaces = NetworkInterface.GetAllNetworkInterfaces();

            int i = 0;
            foreach (var item in Interfaces)
            {
                //Read Data
                IPv4InterfaceStatistics interfaceStats = item.GetIPv4Statistics();
                int bytesSent = (int)interfaceStats.BytesSent;

                //Durschschnittsgeschwindigkeit pro Sekunde
                Thread.Sleep(1000);

                interfaceStats = item.GetIPv4Statistics();
                int newBytesSent = (int)interfaceStats.BytesSent;

                //Read Speed
                arrInterfaces[i].SendSpeed = Math.Abs(bytesSent - newBytesSent) / 1048576D;
                i++;
            }
        }
    }
}