﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using Network;
using Network.AgentÎnformation;

namespace Server
{
    static class Program
    {
        private static AsyncTcpServer _server;

        private static readonly List<Agent> Agents = 
            new List<Agent>();
        private static readonly List<Data> Data = 
            new List<Data>();

        [STAThread]
        private static void Main()
        {
            FormsNotifyIcon.CreateNotifyIcon("Projektwoche 2018 - Server");

            _server = new AsyncTcpServer(IPAddress.Parse("127.0.0.1"), 12);
            _server.PacketReceived += ServerOnPacketReceived;
            _server.ClientConnected += ServerOnClientConnected;
            _server.Start();

            Application.Run();
        }

        private static void ServerOnClientConnected(object sender, ClientConnectedEventArgs e)
        {
            // Wir wissen noch nicht, ob es ein Administrator oder Agent ist!
            // Wir müssen aber davon ausgehen, dass es ein Administrator ist.
            SendAgentPackets(e.Client);
        }

        private static void ServerOnPacketReceived(object sender, PacketReceivedEventArgs e)
        {
            try
            {
                var packetObject = e.Packet.GetObject<Data>();
                    if (packetObject != null)
                {
                    var index = Data.IndexOf(packetObject);
                    if (index <= 0)
                        Data.Add(packetObject);
                    else
                        Data[index] = packetObject;

                    // Die DATA-Klasse vom Agents direkt zum
                    // Administrator weiterleiten
                    _server.SendPacket(packetObject.ClientIndex, new Packet(packetObject));

                    return;
                }

                var packetString = e.Packet.String.ToLower();
                // LOGIN PACKET
                if (packetString.StartsWith("login"))
                {
                    var data = packetString.Split(Packet.DataSeparator);

                    var username = data[1];
                    var password = data[2];

                    if (username == "admin" &&
                        password == "test")
                        _server.SendPacket(e.Sender, new Packet("success"));
                    else
                        _server.SendPacket(e.Sender, new Packet("badlogin"));
                }
                // HELLO PACKET
                else if (packetString.StartsWith("hello")) // Ein Agent ist connected!
                {
                    var data = packetString.Split(Packet.DataSeparator);

                    var mac = data[1];
                    var name = data[2];

                    Agents.Add(new Agent(e.Sender, mac, name));

                    SendAgentPackets(); 
                    // Allen Clients mitteilen, dass ein neuer Agent verbunden ist
                }
                // REQUEST PACKET
                else if (packetString.StartsWith("request"))
                {
                    var data = packetString.Split(Packet.DataSeparator);

                    var clientIndex = _server.GetClientIndex(e.Sender);
                    var agentName = data[1]; // AgentName
                    var index = Agents.FindIndex(agent => agent.AgentName == agentName);

                    if (index >= 0)
                    {
                        // REQUEST Packet zum Agent senden,
                        // der schickt dann DATA zurück
                        _server.SendPacket(Agents[index].Client, 
                            new Packet($"REQUEST{Packet.DataSeparator}{clientIndex}"));
                    }
                }
            }
            catch
            {
                // ignored
            }
        }

        /// <summary>
        /// Methode, die alle Agents zu den Clienten schickt.
        /// Sollte bei Verbinden von einem Agents ausgeführt werden.
        /// </summary>
        private static void SendAgentPackets(TcpClient client = null)
        {
            StringBuilder packetString = new StringBuilder();

            if (Agents.Count == 0) return;

            // Agents als "mehrere" Packete in einem schicken
            foreach (var agent in Agents)
            {
                packetString.Append($"{agent}{'\t'}");
            }

            packetString.Length--;

            if (string.IsNullOrEmpty(packetString.ToString())) return;

            var packet = new Packet(packetString.ToString());

            if (client == null)
                _server.SendPacket(packet);
            else 
                _server.SendPacket(client, packet);
        }
    }
}
