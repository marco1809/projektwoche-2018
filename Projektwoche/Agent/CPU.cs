﻿using System.Diagnostics;
using System.Management;

namespace Agent
{
    class CPU
    {
        public int Processors { get; private set; }
        public string ProcessorID { get; private set; }
        public int Cores { get; private set; }
        public double ClockSpeed { get; private set; }
        public double MaxClockSpeed { get; private set; }
        public double CPUPerformance { get; private set; }
        public double[] CorePerformance { get; private set; }

        //Klasse mit statischen Methoden Objekt wird später übergeben in die Data Klasse für das Serialisieren
        public CPU(int Processors, int Cores, double ClockSpeed, double MaxClockSpeed,  double CPUPerformance, double[] CorePerformance)
        {
            this.ProcessorID = GetProcessorID();
            this.Processors = Processors;
            this.Cores = Cores;
            this.ClockSpeed = ClockSpeed;
            this.MaxClockSpeed = MaxClockSpeed;
            this.CPUPerformance = CPUPerformance;
            this.CorePerformance = CorePerformance;
        }

        public static CPU GetCPUs() => new CPU(GetCPUProcessor(), GetCPUCores(), GetCPUSpeed(), GetCPUMaxSpeed(), GetCPUPerformance(), GetCoresPerformance());

        public static string GetProcessorID()
        {
            string processorid = "";

            foreach (var item in new System.Management.ManagementObjectSearcher("Select ProcessorID from win32_processor").Get())
                processorid = item["processorID"].ToString();

            return processorid;
        }

        public static int GetCPUProcessor()
        {
            int numberofprocessors = 0;

            foreach (var item in new System.Management.ManagementObjectSearcher("Select NumberOfProcessors from Win32_ComputerSystem").Get())
                numberofprocessors = int.Parse(item["NumberOfProcessors"].ToString());

            return numberofprocessors;
        }

        public static int GetCPUCores()
        {
            int numberoflogicalprocessors = 0;

            foreach (var item in new ManagementObjectSearcher("Select NumberOfLogicalProcessors from Win32_Processor").Get())
                numberoflogicalprocessors = int.Parse(item["NumberOfLogicalProcessors"].ToString());

            return numberoflogicalprocessors;
        }

        public static double GetCPUMaxSpeed()
        {
            double maxclockspeed = 0;

            foreach (var item in new ManagementObjectSearcher("SELECT MaxClockSpeed FROM Win32_Processor").Get())
                maxclockspeed = double.Parse(item["MaxClockSpeed"].ToString()) / 1000;

            return maxclockspeed;
        }

        public static double GetCPUSpeed()
        {
            double currentclockspeed = 0;

            foreach (var item in new ManagementObjectSearcher("SELECT CurrentClockSpeed FROM Win32_Processor").Get())
                currentclockspeed = double.Parse(item["CurrentClockSpeed"].ToString()) / 1000;

            return currentclockspeed;
        }

        public static double GetCPUPerformance(string InstanceName = "_Total")
        {
            PerformanceCounter cpuperformance = new PerformanceCounter("Processor", "% Processor Time", InstanceName);
            dynamic firstValue = cpuperformance.NextValue();
            System.Threading.Thread.Sleep(1000);
            dynamic secondValue = cpuperformance.NextValue();

            return secondValue;
        }

        public static double[] GetCoresPerformance()
        {
            int numberoflogicalprocessors = GetCPUCores();

            double[] performanceofcore = new double[numberoflogicalprocessors];
            for (int i = 0; i < numberoflogicalprocessors; i++)
                performanceofcore[i] = GetCPUPerformance(i.ToString());

            return performanceofcore;
        }
    }
}
