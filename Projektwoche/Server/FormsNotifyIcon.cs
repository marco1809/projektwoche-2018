﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Server
{
    internal static class FormsNotifyIcon
    {
        public static NotifyIcon Icon { get; private set; }

        public static NotifyIcon CreateNotifyIcon(string text)
        {
            // Kontextmenü erzeugen
            var cm = new ContextMenu();
            
            // Kontextmenüeinträge erzeugen
            var exitMenuItem = new MenuItem
            {
                Index = 1,
                Text = "Beenden"
            };

            exitMenuItem.Click += ExitMenuItemOnClick;
            cm.MenuItems.Add(exitMenuItem);

            // NotifyIcon selbst erzeugen
            Icon = new NotifyIcon
            {
                Icon = new Icon("tray.ico"),
                Text = text,
                Visible = true,
                ContextMenu = cm
            };
            
            Icon.BalloonTipClosed += (sender, e) => 
            {
                var thisIcon = (NotifyIcon)sender;
                thisIcon.Visible = false;
                thisIcon.Dispose();
            };
            
            return Icon;
        }

        private static void ExitMenuItemOnClick(object sender, EventArgs eventArgs)
        {
            Application.Exit();
        }
    }
}
