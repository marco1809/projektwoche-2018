﻿using System.Windows.Media;
using LiveCharts.Wpf;

namespace Admin
{
    public class ChartSection
    {
        /// <summary>
        /// </summary>
        /// <param name="color">Farbe der Sektion (oder des Bereiches)</param>
        /// <param name="startValue">Y-/X-Startwert (von unten bzw. links)</param>
        /// <param name="size">Größe (nach oben bzw. rechts)</param>
        /// <param name="opactiy">Durchsichtigkeit (0.0 bis 1.0)</param>
        public ChartSection(Color color, int startValue, int size, double opactiy)
        {
            Color = color;
            StartValue = startValue;
            Size = size;
            Opactiy = opactiy;
        }

        public double Opactiy { get; }
        public System.Windows.Media.Color Color { get; }
        public int StartValue { get; }
        public int Size { get; }
        public SolidColorBrush Brush => new SolidColorBrush(Color) { Opacity = Opactiy };

        public AxisSection Section => new AxisSection
            {
                Value = StartValue,
                SectionWidth = Size,
                Fill = Brush
            };
    }
}
