﻿using System;
using System.Net.Sockets;

// ReSharper disable once CheckNamespace
namespace Network
{
    public class PacketReceivedEventArgs : EventArgs
    {
        public Packet Packet { get; }
        public TcpClient Sender { get; }

        public PacketReceivedEventArgs(TcpClient sender, byte[] buffer, int length)
        {
            var tmp = new byte[length];
            // Kopiert vom Puffer die tatsächliche Anzahl bytes ins Packetarray.
            Array.Copy(buffer, tmp, length);

            this.Packet = new Packet(tmp);
            this.Sender = sender;
        }

        public PacketReceivedEventArgs(TcpClient sender, Packet p)
        {
            this.Packet = p;
            this.Sender = sender;
        }
    }
}
