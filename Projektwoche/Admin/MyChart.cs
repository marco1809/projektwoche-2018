﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using LiveCharts;
using LiveCharts.Wpf;

namespace Admin
{
    /// <typeparam name="T">Integer oder Double, je nach Darstellung (im MouseOver z.B.)</typeparam>
    internal class MyChart<T>
    {
        private readonly LiveCharts.WinForms.CartesianChart _chartControl;
        private DateTime _chartInitializationTime;
        private int _maxValuesShown; // Anzahl maximaler X-Punkte die Gezeigt werden
        private readonly List<DateTime> _dateTimeList;

        public MyChart(LiveCharts.WinForms.CartesianChart chartControl)
        {
            _chartControl = chartControl;
            _dateTimeList = new List<DateTime>();
        }

        /// <summary>
        /// Initialisiert das Chart Control.
        /// </summary>
        /// <param name="titleX">Text an der X Achse</param>
        /// <param name="titleY">Text an der Y Achse</param>
        /// <param name="prefixX">Datentyp (z.B. % o. MBit)</param>
        /// <param name="minYValue">Kleinstes Wert (Y-Achse)</param>
        /// <param name="maxYValue">Größter Wert (Y-Achse)</param>
        /// <param name="userSections">Sektionen z.B. für Grün-Orange-Rot</param>
        /// <param name="maxValuesShown">Maximale Anzahl an Punkten die Angezeigt werden (auf X-Achse). -1 = Unenedlich</param>
        public void InitializeChart(string titleX, string titleY, string prefixX = "%",
            int minYValue = 0, int maxYValue = 100, List<ChartSection> userSections = null,
            int maxValuesShown = -1)
        {
            var chart = _chartControl;

            _chartInitializationTime = DateTime.Now;
            _maxValuesShown = maxValuesShown;

            chart.LegendLocation = LegendLocation.Right;

            chart.Zoom = ZoomingOptions.None;
            chart.Pan = PanningOptions.None; // Verschieben (quasi nach links o. rechts scrollen) -> maxValuesShown damit Kaputt

            // Alte Achsen löschen
            if (chart.AxisX.Count > 0)
                chart.AxisX.Clear();

            // X-Achse
            chart.AxisX.Add(new Axis
            {
                MinValue = 0,
                MinRange = 3,
                Title = titleX,
                LabelFormatter =
                    value => 
                        value >= 0 && value < _dateTimeList.Count ?
                            _dateTimeList[(int)value].ToString("hh:mm:ss") : ""
            });

            // "Sektionen" der Y-Achse (z.B. Grün-Orange-Rot)
            var sections = new SectionsCollection
            {
                new ChartSection(Color.FromRgb(0, 0, 0), minYValue, 1, .5).Section, // Strich ganz unten
                new ChartSection(Color.FromRgb(0, 0, 0), maxYValue, 1, .5).Section  // Strich ganz oben
            };

            if (userSections != null)
                foreach (var section in userSections)
                {
                    sections.Add(section.Section);
                }

            // Alte Achsen löschen
            if (chart.AxisY.Count > 0)
                chart.AxisY.Clear();

            // Y-Achse
            chart.AxisY.Add(new Axis
            {
                Title = titleY,
                MinValue = minYValue,
                MaxValue = maxYValue,
                LabelFormatter = value => $"{value} {prefixX}",
                Sections = sections
            });

            _chartControl.Series = new SeriesCollection();
        }

        /// <summary>
        /// Fügt einen Punkt auf einer Linie hinzu
        /// </summary>
        /// <param name="seriesIndex">Index der Linie (Series)</param>
        /// <param name="value">Wert</param>
        public void AddPoint(int seriesIndex, T value)
        {
            var series = _chartControl.Series[seriesIndex];
            series.Values.Add(value);

            _dateTimeList.Add(DateTime.Now);

            try
            {
                // Sollten mehr Punkte angezeigt werden als festgelegt
                // wird das MinValue erhöht (der Graph nach Rechts verschoben)
                if (double.IsNaN(_chartControl.AxisX[0].MaxValue) == false)
                {
                    if (_maxValuesShown > 0 &&
                        Math.Abs(_chartControl.AxisX[0].ActualMaxValue - _chartControl.AxisX[0].ActualMinValue) >=
                        _maxValuesShown)
                    {
                        _chartControl.AxisX[0].MinValue++;
                    }
                }
            }
            catch
            {
                Console.WriteLine("Fehler in AddPoint!");
            }
        }

        /// <summary>
        /// Fügt eine neue Zeile zum Graphen zu.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="brush"></param>
        /// <returns></returns>
        public int NewLine(string title, SolidColorBrush brush)
        {
            _chartControl.Series.Add(new LineSeries
            {
                Title = title,
                Stroke = brush,
                Values = new ChartValues<T>(),
                LineSmoothness = 2,
                StrokeThickness = 2,
                PointGeometrySize = 5,
                Fill = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0))
            });

            return _chartControl.Series.Count - 1;
        }

        /// <summary>
        /// Setzt den Zoom zurück (nicht in Benutzung)
        /// </summary>
        public void ResetChartZoom()
        {
            var chart = _chartControl;

            // Reset Zoom
            chart.AxisX[0].MinValue = 0;
            chart.AxisX[0].MaxValue = 100;
        }
    }
}
