﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable InconsistentlySynchronizedField
// ReSharper disable InconsistentNaming
// ReSharper disable UseNullPropagation
// ReSharper disable once CheckNamespace

namespace Network
{
    public class AsyncTcpServer
    {
        /// <summary>
        ///     Konstruktor der IP & Port einzeln akzeptiert.
        /// </summary>
        /// <param name="localaddr"></param>
        /// <param name="port"></param>
        public AsyncTcpServer(IPAddress localaddr, int port)
            : this()
        {
            tcpListener = new TcpListener(localaddr, port);
            Port = port;

            IsActive = true;
        }

        /// <summary>
        ///     Konstruktor der IPEndPoint (Kombination aus IP & Port) akzeptiert.
        /// </summary>
        /// <param name="localEP"></param>
        public AsyncTcpServer(IPEndPoint localEP)
            : this()
        {
            tcpListener = new TcpListener(localEP);

            IsActive = true;
        }

        /// <summary>
        ///     Standard-Konstruktor, nicht von außerhalb aufrufbar.
        /// </summary>
        protected AsyncTcpServer()
        {
            clients = new List<Client>();

            IsActive = true;
        }

        public void Destroy()
        {
            Stop();

            IsActive = false;
        }

        /// <summary>
        ///     Startet den Server.
        /// </summary>
        public void Start()
        {
            try
            {
                // Startet den Server.
                tcpListener.Start();
                // Beginnt mit dem Akzeptieren von Clienten (Asynchron, neuer Thread).
                // Ruft dann "acceptTcpClientCallback" auf.
                tcpListener.BeginAcceptTcpClient(acceptTcpClientCallback, null);
            }
            catch (Exception ex)
            {
                throw new Exception("Der Server konnte nicht gestartet werden.", ex);
            }
        }

        /// <summary>
        ///     Stoppt den Server
        /// </summary>
        public void Stop()
        {
            try
            {
                // Stoppt den Server.
                tcpListener.Stop();

                // Sperrt die clients während der folgenden Aktion.
                // Dadurch kann nicht von anderen Threads auf clients zugegriffen werden.
                lock (clients)
                {
                    // Schließt die Verbindung zu allen Clienten.
                    foreach (var client in clients)
                        client.TcpClient.Client.Disconnect(false);

                    // Leert die clients-Liste.
                    clients.Clear();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Fehler beim Stoppen des Servers.", ex);
            }
        }

        /// <summary>
        ///     Sendet Bytes an alle Clienten.
        /// </summary>
        /// <param name="bytes"></param>
        private void SendBytes(byte[] bytes)
        {
            // Sendet das Packet an jeden Clienten.
            foreach (var client in clients)
                SendPacket(client, new Packet(bytes));
        }

        /// <summary>
        ///     Sendet ein Packet an alle Clients.
        /// </summary>
        /// <param name="packet"></param>
        public void SendPacket(Packet packet)
        {
            lock (clients)
            {
                foreach (var client in clients)
                    SendPacket(client, packet);
            }
        }

        /// <summary>
        ///     Sendet ein Packet an den Clienten.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="packet"></param>
        public void SendPacket(Client client, Packet packet)
        {
            client.LastSentPacket = packet;

            var packetString = packet.ToString(); // <Nachricht><Seperator><Hash>
            var packetBytes = Encoding.UTF8.GetBytes(packetString);

            SendBytesToClient(client.TcpClient, packetBytes);
        }

        public void SendPacket(int clientIndex, Packet packet)
        {
            if (clients.Count > clientIndex) // Client wurde vielleicht bereits disconnected!
                SendPacket(clients[clientIndex], packet);
        }

        /// <summary>
        ///     Sendet ein Packet an den Clienten.
        /// </summary>
        /// <param name="tcpClient"></param>
        /// <param name="packet"></param>
        public void SendPacket(TcpClient tcpClient, Packet packet)
        {
            var client = clients.Find(c => c.TcpClient.Equals(tcpClient));
            client.LastSentPacket = packet;

            var packetString = packet.ToString(); // <Nachricht><Seperator><Hash>
            var packetBytes = Encoding.UTF8.GetBytes(packetString);

            SendBytesToClient(tcpClient, packetBytes);
        }

        /// <summary>
        ///     Schaut ob das Packet ein Fehler-Packet ist
        /// </summary>
        /// <param name="packet">Das Packet</param>
        /// <returns>False = Kein Fehler-Packet</returns>
        private bool IsErrorPacket(Packet packet)
        {
            var packetObject = packet.Object;

            if (packetObject == null)
            {
                var packetString = packet.String;
                if (packetString.StartsWith("error"))
                    return true;
            }

            return false;
        }

        public int GetClientIndex(TcpClient client)
        {
            lock (clients)
            {
                return clients.FindIndex(x => x.TcpClient.Equals(client));
            }
        }

        private void ResendPacket(Client client, Packet packet)
        {
            if (client != null)
                SendPacket(client, client.LastSentPacket);
        }
        
        /// <summary>
        ///     Sendet ein Packet an einen Clienten.
        /// </summary>
        /// <param name="tcpClient"></param>
        /// <param name="bytes"></param>
        internal void SendBytesToClient(TcpClient tcpClient, byte[] bytes)
        {
            try
            {
                if (tcpClient != null && tcpClient.Connected)
                {
                    var sendBytes = Rijndael.Encrypt(bytes);

                    lock (_testLock)
                    {
                        _testErrorCounter++;
                        if (_testErrorCounter >= 10)
                        {
                            bytes[bytes.Length - 5] = (byte)'\0';

                            _testErrorCounter = 0;
                        }
                    }


                    // Holt den Stream und beginnt einen asynchronen Schreibvorgang (neuer Thread).
                    // Es wird "sendCallback" mit dem Client, an den gesendet werden soll, 
                    // als Parameter "result" uebergeben.
                    var networkStream = tcpClient.GetStream();
                    networkStream.BeginWrite(sendBytes, 0, sendBytes.Length, sendCallback, tcpClient);
                }
                else
                {
                    // Wenn der Client nicht mehr verbunden war,
                    // wird er aus der clients-Liste entfernt.
                    var client = clients.Find(c => c.TcpClient == tcpClient);
                    lock (clients)
                    {
                        clients.Remove(client);
                    }
                    OnClientDisconnected(new ClientDisconnectedEventArgs(client.TcpClient));
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Fehler beim Senden des Packetes.", ex);
            }
        }

        #region Delegates

        public delegate void PacketReceivedEventHandler(object sender, PacketReceivedEventArgs e);

        public delegate void ClientConnectedEventHandler(object sender, ClientConnectedEventArgs e);

        public delegate void ClientDisconnectedEventHandler(object sender, ClientDisconnectedEventArgs e);

        #endregion Delegates

        #region Events

        public event PacketReceivedEventHandler PacketReceived;
        public event PacketReceivedEventHandler InvalidPacketReceived;
        public event ClientConnectedEventHandler ClientConnected;
        public event ClientDisconnectedEventHandler ClientDisconnected;

        #endregion Events

        #region Event Raiser

        private void OnPacketReceived(PacketReceivedEventArgs e)
        {
            var handler = PacketReceived;
            if (handler != null)
                handler(this, e);
        }

        private void OnInvalidPacketReceived(PacketReceivedEventArgs e)
        {
            var handler = InvalidPacketReceived;
            if (handler != null)
                handler(this, e);
        }

        private void OnClientConnected(ClientConnectedEventArgs e)
        {
            var handler = ClientConnected;
            if (handler != null)
                handler(this, e);
        }

        private void OnClientDisconnected(ClientDisconnectedEventArgs e)
        {
            var handler = ClientDisconnected;
            if (handler != null)
                handler(this, e);
        }

        #endregion Event Raiser

        #region Fields

        private readonly TcpListener tcpListener;
        private readonly List<Client> clients;

        #endregion Fields

        #region Properties

        // notwendig, um clients in TcpClients mit foreach 
        // durchlaufen zu koennen
        public IEnumerable<TcpClient> TcpClients
        {
            get
            {
                foreach (var client in clients)
                    yield return client.TcpClient;
            }
        }

        public int Port { get; private set; }

        public int NumberOfConnectedClients => clients.Count;

        public bool IsActive { get; private set; }

        private int _testErrorCounter = 0;
        private object _testLock = new object();

        #endregion Properties

        #region Callbacks

        /// <summary>
        ///     Callback-Methode für das Akzeptieren von Clienten.
        ///     Wird aufgerufen wenn ein Client sich verbindet.
        /// </summary>
        /// <param name="result"></param>
        private void acceptTcpClientCallback(IAsyncResult result)
        {
            if (tcpListener.Server != null && tcpListener.Server.IsBound)
            {
                // Verbindungsversuch vom Clienten abschließen
                var tcpClient = tcpListener.EndAcceptTcpClient(result);

                // Puffergrößte für den Clienten setzen.
                var buffer = new byte[tcpClient.ReceiveBufferSize];
                var client = new Client(tcpClient, buffer);

                // clients sperren und Clienten hinzufügen.
                lock (clients)
                {
                    clients.Add(client);
                }

                // Stream von Clienten holen und Lesevorgang für den Clienten starten.
                // "readCallback" wird mit dem client in "result" aufgerufen.
                var networkStream = client.Networkstream;
                networkStream.BeginRead(client.Buffer, 0, client.Buffer.Length, readCallback, client);

                // Akzeptieren neuer Clienten starten.
                tcpListener.BeginAcceptTcpClient(acceptTcpClientCallback, null);
                // ClientConnected Event ausführen.
                OnClientConnected(new ClientConnectedEventArgs(tcpClient));
            }
        }

        /// <summary>
        ///     Callback-Methode zum Senden von Nachrichten.
        /// </summary>
        /// <param name="result"></param>
        private void sendCallback(IAsyncResult result)
        {
            try
            {
                // Client an den gesendet wurde wird als result übergeben.
                var tcpClient = result.AsyncState as TcpClient;
                // Schreibvorgang abschließen
                var networkStream = tcpClient.GetStream();
                networkStream.EndWrite(result);
            }
            catch (Exception ex)
            {
                throw new Exception("Fehler beim Senden des Packetes.", ex);
            }
        }

        /// <summary>
        ///     Callback-Methode zum Empfangen von Nachrichten.
        /// </summary>
        /// <param name="result"></param>
        private void readCallback(IAsyncResult result)
        {
            // Client von dem Nachricht erhalten wurde wird als result übergeben.
            var client = result.AsyncState as Client;
            Packet packet = null;

            try
            {
                // Prüfung ob Client valide und verbunden ist.
                if (client == null) return;
                if (client.TcpClient.Connected)
                {
                    // Stream holen und Lesevorgang abschließen.
                    var networkStream = client.Networkstream;
                    var read = networkStream.EndRead(result);

                    // Falls 0 bytes gesendet wurden, wird der Client entfernt.
                    if (read == 0)
                        lock (clients)
                        {
                            clients.Remove(client);

                            // ClientDisconnected Event ausführen.
                            OnClientDisconnected(new ClientDisconnectedEventArgs(client.TcpClient));
                            return;
                        }

                    // Tatsächlich empfangene Bytes in neues Array kopieren.
                    var actualEncryptedBytes = new byte[read];
                    Array.Copy(client.Buffer, actualEncryptedBytes, read);

                    var actualBytes = Rijndael.Decrypt(actualEncryptedBytes);

                    // Neuen Lesevorgang mit Clienten starten.
                    client.ClearBuffer();
                    networkStream.BeginRead(client.Buffer, 0, client.Buffer.Length, readCallback, client);

                    // Hier ein Packet-Objekt erstellen
                    // Wird wohlmöglich dieses Format besitzen (in bytes): <Nachricht><Seperator><Hash>
                    packet = Packet.Parse(actualBytes);

                    if (IsErrorPacket(packet))
                    {
                        Console.WriteLine(
                            $"[Server] Received an error Packet: RESENDING! {client.LastSentPacket.String}");

                        ResendPacket(client, packet);
                    }
                    else
                    {
                        Console.WriteLine($"[Server] Received a valid Packet: {packet.String}");

                        OnPacketReceived(new PacketReceivedEventArgs(client.TcpClient, packet));
                    }
                }
            }
            catch (IOException)
            {
                // Bei IO-Fehlern den Client entfernen.
                if (client != null)
                {
                    lock (clients)
                    {
                        clients.Remove(client);
                    }

                    OnClientDisconnected(new ClientDisconnectedEventArgs(client.TcpClient));
                }
            }
            catch
            {
                OnInvalidPacketReceived(new PacketReceivedEventArgs(client.TcpClient, packet));
            }
        }

        #endregion Callbacks
    }
}