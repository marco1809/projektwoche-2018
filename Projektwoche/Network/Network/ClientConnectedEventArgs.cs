﻿using System;
using System.Net.Sockets;

// ReSharper disable once CheckNamespace
namespace Network
{
    public class ClientConnectedEventArgs : EventArgs
    {
        public TcpClient Client { get; }

        public ClientConnectedEventArgs(TcpClient client)
        {
            this.Client = client;
        }
    }
}
