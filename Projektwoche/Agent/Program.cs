﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using Network;

namespace Agent
{
    static class Program
    {
        private static AsyncTcpClient _client;
        private static Data _agentsinformations;

        [STAThread]
        static void Main()
        {
            _client = new AsyncTcpClient();
            _client.Connected += Client_Connected;
            _client.ConnectionFailed += Client_ConnectionFailed;
            _client.Disconnected += Client_Disconnected;
            _client.PacketReceived += Client_PacketReceived;

            FormsNotifyIcon.CreateNotifyIcon("Projektwoche 2018 - Agent");
            _agentsinformations = new Data();

            _client.Connect(IPAddress.Parse("127.0.0.1"), 12);

            Application.Run();

            //while (true)
            //{
            //    //Console.WriteLine("Prozessor: " + test);
            //    //Console.WriteLine("Prozessor: " + tmp.Processors);
            //    //Console.WriteLine("Core: " + tmp.Cores);
            //    //Console.WriteLine("CPU Auslastung: " + tmp.CPUPerformance);
            //    //Console.WriteLine("CPU MaxGeschwindigkeit: " + tmp.MaxClockSpeed);
            //    //Console.WriteLine("Core1:  " + tmp.CorePerformance[0]);
            //    //Console.WriteLine("Core2: " + tmp.CorePerformance[1]);
            //    //Console.WriteLine("Core3: " + tmp.CorePerformance[2]);
            //    //Console.WriteLine("Core4: " + tmp.CorePerformance[3]);

            //    //Console.WriteLine("Letter: " + tmp[0].Letter);
            //    //Console.WriteLine("Name: " + tmp[0].Name);
            //    //Console.WriteLine("Speicher: " + tmp[0].Space);
            //    //Console.WriteLine("Freier Speicher: " + tmp[0].FreeSpace);

            //    //Console.WriteLine("RAM Speicher: " + tmp.Size);
            //    //Console.WriteLine("RAM Nutzung: " + tmp.Used);

            //    //var net = Network2.getInstance();

            //    //System.Threading.Thread.Sleep(1000);
            //    //var net2 = Network2.getInstance();

            //    //var net = new AVG();
            //    //var net2 = AVG.GetInterfaces();
            //    //int i = 0;

            //    //while (true)
            //    //{
            //    //    System.Threading.Thread.Sleep(1000);
            //    //    Console.WriteLine("Send: " + net.GetSentSpeed(net2[3]));
            //    //    Console.WriteLine("Received: " + net.GetReceivedSpeed(net2[3]));
            //    //}

            //    //foreach (var item in test)
            //    //{
            //    //    System.Threading.Thread.Sleep(10000);
            //    //    Console.WriteLine("Name: " + item.Name);
            //    //    Console.WriteLine("Interface: " + item.NetworkInterfaceType);
            //    //    Console.WriteLine("Status: " + item.OperationalStatus);
            //    //    Console.WriteLine("Speed: " + item.Speed);
            //    //    Console.WriteLine("Revceived: " + net2.getReceivedSpeed(item).ToString());
            //    //}


            //    //tmp.getReceivedSpeed(System.Net.NetworkInformation.NetworkInterface );




            //    System.Threading.Thread.Sleep(10000);
            //}

        }

        private static void Client_PacketReceived(object sender, PacketReceivedEventArgs e)
        {
            var packetString = e.Packet.String.ToLower();
            if (packetString.StartsWith("request"))
            {
                var packetSplit = packetString.Split(Packet.DataSeparator);
                var clientIndex = packetSplit[1];
                _agentsinformations.ClientIndex = int.Parse(clientIndex);
                _agentsinformations.Refresh();

                _client.SendPacket(new Packet(_agentsinformations));
            }
        }
        private static void Client_Disconnected(object sender, EventArgs e) => Application.Exit();
        private static void Client_ConnectionFailed(object sender, EventArgs e) => Application.Exit();
        private static void Client_Connected(object sender, ClientConnectedEventArgs e) => _client.SendPacket(new Packet($"HELLO{Packet.DataSeparator}{_agentsinformations._Adapter.arrInterfaces[0]}{Packet.DataSeparator}{Environment.MachineName}"));
    }
}
