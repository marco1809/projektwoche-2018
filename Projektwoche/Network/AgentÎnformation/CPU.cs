﻿namespace Network.AgentÎnformation
{
    public class CPU
    {
        public int Processors { get;  set; }
        public string ProcessorID { get;  set; }
        public int Cores { get;  set; }
        public double ClockSpeed { get;  set; }
        public double MaxClockSpeed { get;  set; }
        public double CPUPerformance { get;  set; }
        public double[] CorePerformance { get;  set; }   
    }
}
