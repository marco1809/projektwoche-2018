﻿using System;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

// ReSharper disable once CheckNamespace
namespace Network
{
    public class Paket
    {
        public const char Trennzeichen = '\n';

        // Bytes des Packets, OHNE HASH
        public byte[] Bytes { get; }
        // Paket als String
        public string String => Encoding.UTF8.GetString(Bytes);
        // Paket als Objekt, null wenn kein (JSON)-Objekt
        public object Object =>
            String.StartsWith("{") ? JsonConvert.DeserializeObject(String) : null;

        public Paket(byte[] bytes)
        {
            Bytes = bytes;
        }

        public Paket(string text) : this(Encoding.UTF8.GetBytes(text))
        {
        }

        public Paket(object obj) : this(JsonConvert.SerializeObject(obj))
        {
        }

        public string GetHash()
        {
            return Hash(Bytes);
        }

        public override string ToString()
        {
            return $"{String}{Trennzeichen}{GetHash()}";
        }

        #region Static Methoden
        
        internal static string Hash(byte[] temp)
        {
            using (var sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(temp);
                return Convert.ToBase64String(hash);
            }
        }

        public static string Hash(string stringToHash)
        {
            return Hash(Encoding.UTF8.GetBytes(stringToHash));
        }

        #endregion
    }
}
