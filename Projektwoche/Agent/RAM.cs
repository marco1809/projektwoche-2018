﻿using System;
using System.Diagnostics;
using System.Management;

namespace Agent
{
    class RAM
    {
        public double Size { get; private set; }
        public double Used { get; private set; }

        //Klasse mit statischen Methoden Objekt wird später übergeben in die Data Klasse für das Serialisieren
        public RAM(double Size, double Used)
        {
            this.Size = Size;
            this.Used = Used;
        }

        public static RAM GetRam() => new RAM(GetRAMSize(), GetRAMPerformance());

        public static double GetRAMSize()
        {
            double totalphysicalmemory = 0;
            foreach (var item in new ManagementObjectSearcher("SELECT TotalPhysicalMemory FROM Win32_ComputerSystem").Get())
                totalphysicalmemory = double.Parse(item["TotalPhysicalMemory"].ToString()) / 1048576;

            return totalphysicalmemory;
        }

        public static double GetRAMPerformance() => Convert.ToDouble(new PerformanceCounter("Memory", "Available MBytes").NextValue());

    }
}
