﻿using System.Net.Sockets;

namespace Network
{
    public class Agent
    {
        public string MacAddress { get; }
        public string AgentName { get; }
        public bool IsOnline => Client != null && Client.Connected;
        public TcpClient Client { get; set; }

        public Agent(TcpClient client, string mac, string name)
        {
            Client = client;
            MacAddress = mac;
            AgentName = name;
        }

        public override string ToString()
        {
            return $"AGENT{Packet.DataSeparator}{MacAddress}{Packet.DataSeparator}{AgentName}";
        }
    }
}
