﻿namespace Network.AgentÎnformation
{
    public class Disk
    {
        public string Letter { get;  set; }
        public string Name { get;  set; }
        public double Space { get;  set; }
        public double FreeSpace { get;  set; }
    }
}
