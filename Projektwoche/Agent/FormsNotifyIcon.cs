﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Agent
{
    internal static class FormsNotifyIcon
    {
        public static NotifyIcon Icon { get; private set; }


        public static NotifyIcon CreateNotifyIcon(string text)
        {
            var cm = new ContextMenu();

            var exitMenuItem = new MenuItem
            {
                Index = 1,
                Text = "Beenden"
            };

            exitMenuItem.Click += ExitMenuItemOnClick;
            cm.MenuItems.Add(exitMenuItem);

            return Icon = new NotifyIcon
            {
                //Icon = new Icon("tray.ico"),
                Text = text,
                Visible = true,
                ContextMenu = cm
            };
        }
        private static void ExitMenuItemOnClick(object sender, EventArgs eventArgs) => Application.Exit();
    }
}
