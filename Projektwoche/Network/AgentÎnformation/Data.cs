﻿namespace Network.AgentÎnformation
{
    public class Data
    {
        public Adapter _Adapter { get; set; }
        public CPU _CPU { get; set; }
        public Disk[] _Disk { get; set; }
        public RAM _RAM { get; set; }
        public string AgentName { get; set; }
        public int ClientIndex { get; set; }

        public Data()
        {

        }
    }
}
