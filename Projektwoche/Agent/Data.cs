﻿using System;

namespace Agent
{
    class Data
    {
        public Adapter _Adapter { get; private set; }
        public CPU _CPU { get; private set; }
        public Disk[] _Disk { get; private set; }
        public RAM _RAM { get; private set; }
        public string AgentName { get; private set; }
        public int ClientIndex { get; set; }

        //Diese Klasse wird übergeben
        public Data()
        {
            _Adapter = new Adapter();
            AgentName = Environment.MachineName;
            Refresh();
        }

        public void Refresh()
        {
            _CPU = CPU.GetCPUs();
            _Disk = Disk.GetDisks();
            _RAM = RAM.GetRam();
        }
    }
}
