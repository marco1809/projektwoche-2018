﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;

// ReSharper disable once CheckNamespace
namespace Network
{
    public class Client
    {
        public Packet LastSentPacket { get; set; }

        public Client(TcpClient tcpClient, byte[] buffer)
        {
            this.TcpClient = tcpClient;// ?? throw new ArgumentNullException(nameof(tcpClient));
            this.Buffer = buffer;// ?? throw new ArgumentNullException(nameof(buffer));

            //SentPackets = new Dictionary<int, string>();
        }

        public TcpClient TcpClient { get; private set; }

        public byte[] Buffer { get; private set; }

        public NetworkStream Networkstream => TcpClient.GetStream();

        public void ClearBuffer()
        {
            Buffer = new byte[TcpClient.ReceiveBufferSize];
        }

        //public int AddPacket(string packet)
        //{
        //    var index = 0;

        //    if (SentPackets.Count > 0)
        //        index = SentPackets.Keys.Last() + 1;

        //    SentPackets.Add(index, packet);

        //    return index;
        //}
    }
}
