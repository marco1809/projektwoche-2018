﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Media;
using LiveCharts.Defaults;
using LiveCharts.WinForms;
using Network;
using Network.AgentÎnformation;
using Color = System.Windows.Media.Color;

namespace Admin
{
    public partial class FrmAdmin : Form
    {
        private FrmConnect _frmConnect;
        
        private AsyncTcpClient _client;
        private Graph _graph;

        private readonly List<Agent> _agents = new List<Agent>();
        
        private int _agentId = 0;
        private bool _requestAllAgents = false; // muss noch eingebaut werden
        private bool _firstDataAfterChange = true;

        public FrmAdmin()
        {
            InitializeComponent();
        }

        #region Form related stuff

        private void FrmAdmin_Load(object sender, EventArgs e)
        {
            SetConnectionStatus("Nicht verbunden!", System.Drawing.Color.Red);
        }

        private void SetConnectionStatus(string text, System.Drawing.Color color)
        {
            Invoke((MethodInvoker) (() =>
            {
                lbConnectionStatus.Text = text;
                lbConnectionStatus.BackColor = color;
            }));
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            // Nur wenn keine Verbindung besteht in frmConnect wechseln
            if (_client != null && _client.IsConnected)
            {
                var msgBoxResult = 
                    MessageBox.Show("Bestehende Verbindung", "Möchten Sie die bestehende Verbindung trennen?", MessageBoxButtons.YesNo);

                if (msgBoxResult == DialogResult.No) return;

                _client.Destroy();
            }

            _frmConnect = new FrmConnect();
            _frmConnect.Client.PacketReceived += ClientOnPacketReceived;

            var result = _frmConnect.ShowDialog();

            if (result == DialogResult.OK)
            {
                _client = _frmConnect.Client;
                _client.Disconnected += ClientOnDisconnected;
                SetConnectionStatus("Erfolgreich verbunden!", System.Drawing.Color.Green);

                btnConnect.Enabled = false;
                cbAgent.Enabled = true;
            }
            else
            {
                SetConnectionStatus("Keine Verbindung", System.Drawing.Color.Red);
            }
        }
        
        private void cbAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_client.IsActive)
            {
                _agentId = cbAgent.SelectedIndex;
                _requestAllAgents = false;
                _firstDataAfterChange = true;

                // Fokus auf die Form legen.

                ClearCharts();
            }
        }
        
        #endregion

        private void ClientOnPacketReceived(object sender, PacketReceivedEventArgs e)
        {
            var packetObject = e.Packet.GetObject<Data>();
            if (packetObject != null)
            {
                if (_requestAllAgents || (_agents.Count >= 0 &&
                    packetObject.AgentName.ToLower().Equals(_agents[_agentId].AgentName)))
                {
                    Invoke((MethodInvoker) (() =>
                    {
                        if (_firstDataAfterChange)
                        {
                            ClearCharts();
                            _firstDataAfterChange = false;
                            _graph = new Graph(packetObject, cCpu, cRam, new[] {sgDisk1, sg2}, cNetwork);
                        }

                        try
                        {
                            _graph.Refresh(packetObject);

                            lblClockSpeedValue.Text = $"{packetObject._CPU.ClockSpeed} GHz";
                            lblMaxClockSpeedValue.Text = $"{packetObject._CPU.MaxClockSpeed} GHz";
                            lblProcessorCountValue.Text = $"{packetObject._CPU.Processors}";
                            lblCoresValue.Text = $"{packetObject._CPU.Cores}";

                            if (packetObject._Disk.Length >= 1)
                                lblDiskTitle1.Text = $"({packetObject._Disk[0].Letter}) {packetObject._Disk[0].Name}";

                            if (packetObject._Disk.Length >= 2)
                                lblDiskTitle2.Text = $"({packetObject._Disk[1].Letter}) {packetObject._Disk[1].Name}";
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine($"Fehler beim Refresh: {ex.Message}");
                        }
                    }));
                }

                return;
            }

            var packetString = e.Packet.String.ToLower();

            if (packetString.StartsWith("agent"))
            {
                var packets = packetString.Split('\t');

                foreach (var packet in packets)
                {
                    var data = packet.Split(Packet.DataSeparator);
                    var mac = data[1];
                    var name = data[2];
                    var agent = new Agent(e.Sender, mac, name);
                    AddAgent(agent);
                }
            }
        }

        private void ClientOnDisconnected(object sender, EventArgs eventArgs)
        {
            SetConnectionStatus("Verbindung unterbrochen!", System.Drawing.Color.Red);

            Invoke((MethodInvoker)(() =>
            {
                btnConnect.Enabled = true;
                cbAgent.Enabled = false;
            }));
        }

        /// <summary>
        /// Fügt einen Agent die Agents Liste hinzu und fügt ihn in die
        /// ComboBox hinzu.
        /// </summary>
        /// <param name="agent"></param>
        private void AddAgent(Agent agent)
        {
            if (_agents.Contains(agent) == false)
            {
                _agents.Add(agent);

                Invoke((MethodInvoker)(() => cbAgent.Items.Add(agent.AgentName)));
            }
        }

        /// <summary>
        /// Methode um ein REQUEST Paket abzuschicken.
        /// </summary>
        /// <param name="a"></param>
        private void RequestData(Agent a)
        {
            var packet = new Packet($"REQUEST{Packet.DataSeparator}{a.AgentName}");
            _client.SendPacket(packet);
        }

        /// <summary>
        /// Sendet alle X Sekunden ein REQUEST Paket.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timDataRequest_Tick(object sender, EventArgs e)
        {
            if (_agents.Count == 0) return;

            try
            {
                RequestData(_agents[_agentId]);

                if (_requestAllAgents)
                    if (_agentId < _agents.Count)
                        _agentId++;
                    else
                        _agentId = 0;                
            }
            catch (Exception)
            {
                MessageBox.Show("Bei dem Client {0} ist ein Fehler aufgetreten!", _agents[_agentId].AgentName);
            }
        }

        /// <summary>
        /// Leert die Charts.
        /// </summary>
        private void ClearCharts()
        {
            foreach (var chart in new[] { cCpu, cNetwork, cRam })
            {
                foreach (var series in chart.Series)
                {
                    series.Values.Clear();
                    series.Erase(true);
                }
                chart.Series.Clear();
            }
            sgDisk1.Value = sg2.Value = 0;
        }
    }
}
