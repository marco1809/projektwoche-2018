﻿namespace Network.Messages
{
    class TestObjekt : Message
    {
        public string TestString { get; }
        private string _privateString;

        //public TestObjekt()
        //{

        //}

        public TestObjekt(string testString, string privateString, double testDouble,
            string messageType) : base(messageType)
        {
            TestString = testString;
            _privateString = privateString;
            TestDouble = testDouble;
        }

        public double TestDouble { get; }
    }
}
