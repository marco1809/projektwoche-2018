﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Network;

namespace Admin
{
    public partial class FrmConnect : Form
    {
        public AsyncTcpClient Client { get; private set; }

        public string Username { get; private set; }
        public string Password { get; private set; }
        public IPAddress Ip { get; private set; }
        public int Port { get; private set; }

        public FrmConnect()
        {
            InitializeComponent();
            Client = new AsyncTcpClient();
        }

        private void FrmConnect_Load(object sender, EventArgs e)
        {
            cbServerIp.SelectedIndex = 0;
            cbServerPort.SelectedIndex = 0;
            
            Client.Connected += ClientOnConnected;
            Client.ConnectionFailed += ClientOnConnectionFailed;
            Client.Disconnected += ClientOnDisconnected;
            Client.PacketReceived += ClientOnPacketReceived;
        }

        private void ClientOnPacketReceived(object sender, PacketReceivedEventArgs e)
        {
            if (e.Packet.String.StartsWith("success"))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                SetStatus("Unbekannter Benutzer / Password.", Color.Red);

                SetEnabled(true);
            }
        }

        private void ClientOnDisconnected(object sender, EventArgs eventArgs)
        {
            SetStatus("Verbindung getrennt!", Color.Red);

            SetEnabled(true);
        }

        private void ClientOnConnectionFailed(object sender, EventArgs eventArgs)
        {
            SetStatus("Verbindung fehlgeschlagen!", Color.Red);

            SetEnabled(true);
        }

        private void ClientOnConnected(object sender, ClientConnectedEventArgs clientConnectedEventArgs)
        {
            SetStatus("Erfolgreich verbunden! Einloggen...", Color.Orange);

            SetEnabled(false);

            SendLoginPacket();
        }

        private void SendLoginPacket()
        {
            Client.SendPacket(new Packet($"LOGIN{Packet.DataSeparator}{Username}{Packet.DataSeparator}{Password}"));
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            SetEnabled(false);

            Username = tbUsername.Text;
            Password = tbPasswort.Text;

            if (IPAddress.TryParse(cbServerIp.Text, out var ip) &&
                int.TryParse(cbServerPort.Text, out var port) &&
                string.IsNullOrEmpty(Username) == false &&
                string.IsNullOrEmpty(Password) == false)
            {
                Ip = ip;
                Port = port;

                SetStatus("Verbinde...", Color.Transparent);

                if (Client.IsConnected == false)
                    Client.Connect(ip, port);
                else
                    SendLoginPacket();
            }
            else
            {
                SetStatus("Ungültige eingabe!", Color.Red);

                SetEnabled(true);
            }
        }

        private void FrmConnect_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Nicht mehr gebrauchte Event unsubscriben (nicht umbedingt nötig)
            Client.Disconnected -= ClientOnDisconnected;
            // Nur das erste Packet (mit Antwort der Anmeldung) empfangen
            Client.PacketReceived -= ClientOnPacketReceived;

            // FormClosing wird "immer" aufgerufen, deswegen schauen, ob der User
            // die Form geschlossen hat oder die Form durch ein Ereignis (z.B. erfoglreicher
            // Login) geschlossen wrude
            if (DialogResult == DialogResult.None)
            {
                Client.Destroy();

                DialogResult = DialogResult.Abort;
            }

            e.Cancel = false;
        }

        private void SetEnabled(bool enabled)
        {
            if (this.IsDisposed == false && this.IsAccessible)
                Invoke((MethodInvoker)(() =>
                {
                    btnConnect.Enabled = enabled;
                    tbUsername.Enabled = enabled;
                    tbPasswort.Enabled = enabled;
                    cbServerIp.Enabled = enabled;
                    cbServerPort.Enabled = enabled;
                }));
            }

        private void SetStatus(string text, Color color)
        {
            if (this.IsDisposed == false && this.IsAccessible)
                Invoke((MethodInvoker) (() =>
                {
                    lbStatus.Text = text;
                    lbStatus.BackColor = color;
                }));
        }
    }
}
