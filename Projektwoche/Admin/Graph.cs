﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using Network.AgentÎnformation;
using Color = System.Windows.Media.Color;
using LiveCharts.WinForms;

namespace Admin
{
    internal class Graph
    {
        //Eigenschaften für die WIndows Forms Objekte
        public MyChart<double> CPU_Graph { get; private set; }
        public MyChart<double> RAM_Graph { get; private set; }
        public SolidGauge[] Disk_Graph { get; private set; }
        public MyChart<double> Network_Graph { get; private set; }
        
        //Array mit Farben, Füllen, Random wäre unpassend 
        private Color[] Colors = new[] { Color.FromRgb(0, 0, 0), Color.FromRgb(20, 20, 150), Color.FromRgb(150, 10, 130), Color.FromRgb(100, 100, 50),
            Color.FromRgb(140, 90, 30), Color.FromRgb(255, 255, 255), Color.FromRgb(250, 0, 50), Color.FromRgb(200, 200, 200)};
        private int i;

        public Graph(Data Agent_Packet, CartesianChart cpu, CartesianChart ram, SolidGauge[] disk, CartesianChart network, int network_max = 100)
        {
            #region Create CPU Graph
            this.CPU_Graph = new MyChart<double>(cpu);
            this.CPU_Graph.InitializeChart("Zeit", "Auslastung", "%", 0, 100,
                new List<ChartSection>
                {
                    new ChartSection(Color.FromRgb(25, 255, 50), 0, 50, .25),
                    new ChartSection(Color.FromRgb(255, 125, 25), 50, 30, .25),
                    new ChartSection(Color.FromRgb(254, 12, 1), 80, 20, .25)
                }, 20);


            this.CPU_Graph.NewLine("Performance", new SolidColorBrush(Colors[0]));
            int i = 0;
            foreach (var item in Agent_Packet._CPU.CorePerformance)
                this.CPU_Graph.NewLine("Core_" + i, new SolidColorBrush(Colors[1 + i++]));
            #endregion
            #region Create RAM Graph
            this.RAM_Graph = new MyChart<double>(ram);
            this.RAM_Graph.InitializeChart("Zeit", "Speicher", "MB", 0, Convert.ToInt32(Agent_Packet._RAM.Size),
               new List<ChartSection>
               {
                    new ChartSection(Color.FromRgb(25, 255, 50), 0, Convert.ToInt32(Agent_Packet._RAM.Size * 0.5), .25),
                    new ChartSection(Color.FromRgb(255, 125, 25), Convert.ToInt32(Agent_Packet._RAM.Size * 0.5), Convert.ToInt32(Agent_Packet._RAM.Size * 0.3), .25),
                    new ChartSection(Color.FromRgb(254, 12, 1), Convert.ToInt32(Agent_Packet._RAM.Size * 0.5) + Convert.ToInt32(Agent_Packet._RAM.Size * 0.3), Convert.ToInt32(Agent_Packet._RAM.Size * 0.2), .25)
               }, 20);
            this.RAM_Graph.NewLine("Benutzter Speicher", new SolidColorBrush(Colors[0]));
            #endregion
            #region Create Disk Tacho
            this.Disk_Graph = disk;
            i = 0;
            foreach (var item in Disk_Graph)
            {
                if (Agent_Packet._Disk.Length <= i) break;

                item.From = 0;
                item.To = Agent_Packet._Disk[i++].Space;
                //item.LabelFormatter = 
                //    value => $"{0} mb"; 
            }
            #endregion
            #region Create Network Graph
            this.Network_Graph = new MyChart<double>(network);
            this.Network_Graph.InitializeChart("Zeit", "Auslastung", "Mbit", 0, network_max,
               new List<ChartSection>
               {
                    new ChartSection(Color.FromRgb(25, 255, 50), 0, Convert.ToInt32(network_max * 0.5), .25),
                    new ChartSection(Color.FromRgb(255, 125, 25), 50, Convert.ToInt32(network_max * 0.3), .25),
                    new ChartSection(Color.FromRgb(254, 12, 1), 80, Convert.ToInt32(network_max * 0.2), .25)
               }, 20);

            i = 0;
            foreach (var item in Agent_Packet._Adapter.arrInterfaces)
            {
                this.Network_Graph.NewLine(Agent_Packet._Adapter.arrInterfaces[i].Name,
                    new SolidColorBrush(Colors[i]));

                i++;
            }

            #endregion
        }

        /// <summary>
        /// Lädt die Daten der Graphen neu.
        /// </summary>
        /// <param name="AgentPacket"></param>
        public void Refresh(Data AgentPacket)
        {
            //CPU eintragen
            CPU_Graph.AddPoint(0, AgentPacket._CPU.CPUPerformance);
            for (var i = 0; i < AgentPacket._CPU.Cores; i++)
            {
                CPU_Graph.AddPoint(1 + i, AgentPacket._CPU.CorePerformance[i]);
            }

            //RAM eintragen
            RAM_Graph.AddPoint(0, AgentPacket._RAM.Used);

            //Disk
            for (int j = 0; j < 2; j++)
            {
                if (AgentPacket._Disk.Length > j)
                    Disk_Graph[j].Value = AgentPacket._Disk[j].Space - AgentPacket._Disk[j].FreeSpace;
            }

            //Network
            i = 0;
            foreach (var item in AgentPacket._Adapter.arrInterfaces)
            {
                Network_Graph.AddPoint(i, AgentPacket._Adapter.arrInterfaces[i].ReceivedSpeed);
                i++;
            }
        }
    }
}
