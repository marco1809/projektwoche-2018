﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

// ReSharper disable InconsistentlySynchronizedField
// ReSharper disable InconsistentNaming
// ReSharper disable UseNullPropagation
// ReSharper disable once CheckNamespace

namespace Network
{
    public class AsyncTcpClient
    {
        /// <summary>
        ///     Standard Konstruktor
        /// </summary>
        public AsyncTcpClient()
        {
            TcpClient = new TcpClient();
            // Puffer wird auf maximale Empfangsgröße von Packeten festgelegt.
            _buffer = new byte[TcpClient.ReceiveBufferSize];

            IsActive = true;
        }

        public void Destroy()
        {
            if (IsConnected)
                Disconnect();

            IsActive = false;
        }

        /// <summary>
        ///     Startet einen Verbindungsversuch zum Server.
        ///     Asynchron
        /// </summary>
        /// <param name="localaddr"></param>
        /// <param name="port"></param>
        public void Connect(IPAddress localaddr, int port)
        {
            // Wenn der Client verbunden ist, kein erneuter Verbindungsversuch.
            if (TcpClient.Connected)
                throw new InvalidOperationException();
            try
            {
                // Beginnt einen Asynchronen Verbindungsversuch in einem neuen Thread.
                TcpClient.BeginConnect(localaddr, port, ConnectCallback, null);
            }
            catch (Exception ex)
            {
                throw new Exception("Fehler beim Verbinden zum Server.", ex);
            }
        }

        /// <summary>
        ///     Trennt die Verbindung zum Server.
        ///     Synchron
        /// </summary>
        public void Disconnect()
        {
            // Wenn der Client nicht verbunden ist kann nichts getrennt werden.
            if (!TcpClient.Connected)
                throw new InvalidOperationException();

            TcpClient.Close();

            OnDisconnected(EventArgs.Empty);
        }

        /// <summary>
        ///     Sendet ein Packet an den Server.
        /// </summary>
        /// <param name="packet"></param>
        public void SendPacket(Packet packet)
        {
            var packetString = packet.ToString();
            var packetBytes = Encoding.UTF8.GetBytes(packetString);

            SendBytes(packetBytes);
        }

        /// <summary>
        ///     Sendet Bytes an den Server.
        /// </summary>
        /// <param name="bytes"></param>
        public void SendBytes(byte[] bytes)
        {
            // Wenn der Client nicht verbunden ist kann nichts gesendet werden.
            if (!TcpClient.Connected)
                throw new InvalidOperationException();

            try
            {
                var encryptedBytes = Rijndael.Encrypt(bytes);

                // Stream holen und Asynchron das Packet in den Stream schreiben (neuer Thread).
                var networkStream = TcpClient.GetStream();
                networkStream.BeginWrite(encryptedBytes, 0, encryptedBytes.Length, sendCallback, null);
            }
            catch (Exception ex)
            {
                throw new Exception("Fehler beim Senden des Packetes.", ex);
            }
        }

        #region Delegates

        public delegate void PacketReceivedEventHandler(object sender, PacketReceivedEventArgs e);

        public delegate void ConnectedEventHandler(object sender, ClientConnectedEventArgs e);

        public delegate void DisconnectedEventHandler(object sender, EventArgs e);

        public delegate void ConnectionFailedEventHandler(object sender, EventArgs e);

        #endregion Delegates

        #region Events

        public event PacketReceivedEventHandler InvalidPacketReceived;
        public event PacketReceivedEventHandler PacketReceived;
        public event ConnectedEventHandler Connected;
        public event DisconnectedEventHandler Disconnected;
        public event ConnectionFailedEventHandler ConnectionFailed;

        #endregion Events

        #region Event Raiser

        protected void OnInvalidPacketReceived(PacketReceivedEventArgs e)
        {
            var recHandler = InvalidPacketReceived;
            if (recHandler != null)
                recHandler(this, e);
        }

        protected void OnPacketReceived(PacketReceivedEventArgs e)
        {
            var recHandler = PacketReceived;
            if (recHandler != null)
                recHandler(this, e);
        }

        protected void OnConnected(ClientConnectedEventArgs e)
        {
            var conHandler = Connected;
            if (conHandler != null)
                conHandler(this, e);
        }

        protected void OnDisconnected(EventArgs e)
        {
            var disHandler = Disconnected;
            if (disHandler != null)
                disHandler(this, e);
        }

        protected void OnConnectionFailed(EventArgs e)
        {
            var failHandler = ConnectionFailed;
            if (failHandler != null)
                failHandler(this, e);
        }

        #endregion Event Raiser

        #region Fields

        /// <summary>
        ///     Der zugrundeliegende TCPClient (System.Net.Sockets).
        /// </summary>
        public TcpClient TcpClient { get; }

        /// <summary>
        ///     Der Puffer zum Empfangen von Nachrichten.
        /// </summary>
        private readonly byte[] _buffer;

        #endregion Fields

        #region Properties

        public bool IsActive { get; private set; }

        /// <summary>
        ///     gibt zurück, ob der Socket des Clients mit einem Host verbunden ist
        /// </summary>
        public bool IsConnected => TcpClient.Connected && IsActive;

        public string IPAdrStr
        {
            get
            {
                if (TcpClient.Connected)
                {
                    var part = TcpClient.Client.LocalEndPoint.ToString().Split(':');
                    return part[0];
                }
                return string.Empty;
            }
        }

        public string PortStr
        {
            get
            {
                if (TcpClient.Connected)
                {
                    var part = TcpClient.Client.LocalEndPoint.ToString().Split(':');
                    return part[1];
                }
                return string.Empty;
            }
        }

        #endregion Properties

        #region Public Callbacks

        /// <summary>
        ///     Callback-Methode die nach dem Verbinden zum Server aufgerufen wird.
        /// </summary>
        /// <param name="result"></param>
        private void ConnectCallback(IAsyncResult result)
        {
            try
            {
                // Beendet den Verbindungsversuch und schreibt benötigte Rückgabewerte in result.
                TcpClient.EndConnect(result);

                // Führt das Event Connected aus.

                // Holt den Stream und beginnt einen asynchronen Lesezyklus (neuer Thread).
                var networkStream = TcpClient.GetStream();
                networkStream.BeginRead(_buffer, 0, _buffer.Length, readCallback, null);

                OnConnected(new ClientConnectedEventArgs(TcpClient));
            }
            catch (SocketException)
            {
                OnConnectionFailed(EventArgs.Empty);
            }
            catch (Exception ex)
            {
                throw new Exception("Fehler beim Verbinden zum Server.", ex);
            }
        }

        /// <summary>
        ///     Callback-Methode die nach dem Senden einer Nachricht an den Server aufgerufen wird.
        /// </summary>
        /// <param name="result"></param>
        private void sendCallback(IAsyncResult result)
        {
            try
            {
                // Holt den Stream und beendet den asynchronen Sendevorgang (Nachricht wurde gesendet).
                var networkStream = TcpClient.GetStream();
                networkStream.EndWrite(result);
            }
            catch (Exception ex)
            {
                throw new Exception("Fehler beim Senden des Packetes.", ex);
            }
        }

        /// <summary>
        ///     Callback-Methode die aufgerufen wird sobald eine Nachricht vom Server ankommt.
        /// </summary>
        /// <param name="result"></param>
        private void readCallback(IAsyncResult result)
        {
            if (!TcpClient.Connected)
                return;

            byte[] actualBytes;
            try
            {
                // Holt den Stream und beendet den asynchronen Lesevorgang (Nachricht empfangen).
                // Anzahl der empfangenen Bytes wird in read geschrieben.
                var networkStream = TcpClient.GetStream();

                var read = networkStream.EndRead(result);

                if (read > 0)
                {
                    // Kopiert die Anzahl empfangener Bytes aus dem Puffer (Puffer 8kb).
                    actualBytes = new byte[read];
                    Array.Copy(_buffer, actualBytes, read);

                    // PacketReceived Event ausführen.

                    if (networkStream.CanRead)
                        networkStream.BeginRead(_buffer, 0, _buffer.Length, readCallback, null);
                    else
                        Disconnect();
                }
                else
                {
                    Disconnect();
                    return;
                }
            }
            catch (IOException)
            {
                OnDisconnected(EventArgs.Empty);
                return;
            }

            try
            {
                // Verschlüsselung
                actualBytes = Rijndael.Decrypt(actualBytes);

                Console.WriteLine($"Received: {Encoding.UTF8.GetString(actualBytes)}");
                var packetSplit = actualBytes.Split((byte) Packet.EndChar);

                foreach (var packetBytes in packetSplit)
                {
                    // Hier ein Packet-Objekt erstellen
                    // Wird wohlmöglich dieses Format besitzen (in bytes): <Nachricht><Seperator><Hash>
                    var packet = Packet.Parse(packetBytes);

                    // Überprüft ob der berechnete und übergebene Hash identisch sind (ggn. Störungen)
                    if (packet.Hash.Equals(packet.ComputedHash))
                    {
                        Console.WriteLine($"[Client] Received a valid Packet: {packet.String}");

                        OnPacketReceived(new PacketReceivedEventArgs(null, packet));
                    }
                    else
                    {
                        Console.WriteLine($"[Client] Received an invalid Packet: {packet.String} - ERROR!");

                        SendPacket(new Packet("error"));
                    }
                }
            }
            catch
            {
                OnInvalidPacketReceived(new PacketReceivedEventArgs(TcpClient, actualBytes, actualBytes.Length));
            }
        }

        #endregion Public Callbacks
    }
}