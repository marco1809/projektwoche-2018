﻿namespace Network.AgentÎnformation
{
    public class Netstat
    {
        public string Name { get; set; }
        public string Mac { get; set; }
        public double ReceivedSpeed { get; set; }
        public double SendSpeed { get; set; }
    }
}
