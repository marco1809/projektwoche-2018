﻿namespace Agent
{
    public class Netstat
    {
        //Klasse wird für Adapter verwendet, damit nicht zu viele Eigenschaften serialisiert werden (Adapter)
        public string Name { get; set; }
        public string Mac { get; set; }
        public double ReceivedSpeed { get; set; }
        public double SendSpeed { get; set; }
    }
}
