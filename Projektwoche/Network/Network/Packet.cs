﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// ReSharper disable once CheckNamespace
namespace Network
{
    /// <summary>
    /// Eine Klasse die für die AsyncTcp... Klassen verwendet wird.
    /// Ermöglicht einen einfacheren Zugriff auf das Packet (Bytes) als String & Object.
    /// Außerdem sind die Hash-Komponenten hier drin verbaut, die bei Störungen eingreifen sollen.
    /// </summary>
    public class Packet
    {
        public const char Separator = '\n';     // für Nachricht-Hash
        public const char EndChar = '\r';       // am Ende des Packets
        public const char DataSeparator = '\0'; // Trennzeichen innerhalb der Nachricht

        public byte[] Bytes { get; }
        public string String => Encoding.UTF8.GetString(Bytes);
        internal JObject Object =>
            String.StartsWith("{") ? 
                (JObject)JsonConvert.DeserializeObject(String) : null;

        public string Hash { get; private set; } // Der Hash der entweder mit übergeben wird oder gleich ComputedHash ist
        public string ComputedHash => GetHash();
        
        /// <summary>
        /// Für empfangene Packete siehe: <see cref="Network.Packet.Parse"/>
        /// </summary>
        internal Packet(byte[] messageBytes, byte[] hashBytes = null)
        {
            Bytes = messageBytes;

            if (Hash == null)
                if (hashBytes != null)
                    Hash = Encoding.UTF8.GetString(hashBytes);
                else
                    Hash = ComputedHash;
        }

        /// <summary>
        /// Für empfangene Packete siehe: <see cref="Network.Packet.Parse"/>
        /// </summary>
        public Packet(string text, string hash = null) : this(Encoding.UTF8.GetBytes(text), null)
        {
            if (Hash == null) Hash = hash;
        }

        /// <summary>
        /// Für empfangene Packete siehe: <see cref="Network.Packet.Parse"/>
        /// </summary>
        public Packet(object obj) : this(JsonConvert.SerializeObject(obj))
        {
        }

        private string GetHash()
        {
            return ComputeHash(Bytes);
        }

        /// <summary>
        /// Gibt ein Objekt vom Typ T zurück. Packet.Object liefert ein JsonObject zurück
        /// Mögliche Exception!
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>Objekt vom Typ T oder NULL</returns>
        public T GetObject<T>()
        {
            if (String.StartsWith("{"))
            {
                return JsonConvert.DeserializeObject<T>(String);
            }

            return default(T);
        }

        /// <summary>
        /// Gibt das Packet bereit zum versenden zurück
        /// Format: Nachricht|Hash
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{String}{Separator}{GetHash()}{EndChar}";
        }
        
        #region Static Methoden

        internal static string ComputeHash(byte[] temp)
        {
            using (var sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(temp);
                return Convert.ToBase64String(hash);
            }
        }

        internal static string ComputeHash(string stringToHash)
        {
            return ComputeHash(Encoding.UTF8.GetBytes(stringToHash));
        }

        /// <summary>
        /// Verarbeitet einen Byte-Array und gibt ein Packet-Objekt zurück.
        /// Nötig da ein empfangenes Packet im Format Nachricht|Hash vorliegt.
        /// </summary>
        /// <param name="actualBytes"></param>
        /// <returns></returns>
        public static Packet Parse(byte[] actualBytes)
        {
            Packet packet = null;
            var separatorIndex = Array.LastIndexOf(actualBytes, (byte)Packet.Separator);
            if (separatorIndex >= 0) // Wenn es Packet MIT Hash ist
            {
                // Die ersten X Bytes sind die Nachricht (Inhalt) des Packets
                var messageBytes = new byte[separatorIndex];
                Array.Copy(actualBytes, messageBytes, separatorIndex);
                Array.Reverse(actualBytes);
                
                var hashBytes = new byte[actualBytes.Length - separatorIndex - 1];
                Array.Copy(actualBytes, hashBytes, actualBytes.Length - separatorIndex - 1); // ohne das Trennzeichen
                Array.Reverse(hashBytes);

                packet = new Packet(messageBytes, hashBytes);
            }
            else
                packet = new Packet(actualBytes);

            return packet;
        }

        #endregion
    }
}
