﻿using System;
using System.Net.Sockets;

// ReSharper disable once CheckNamespace
namespace Network
{
    public class ClientDisconnectedEventArgs : EventArgs
    {
        public TcpClient Client { get; }

        public ClientDisconnectedEventArgs(TcpClient client)
        {
            this.Client = client;
        }
    }
}
